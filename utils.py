#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017 J. Manrique Lopez de la Fuente
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
#
# Authors:
#     J. Manrique Lopez <jsmanrique@bitergia.com>
#

import os

from elasticsearch import Elasticsearch, helpers
from elasticsearch_dsl import Search

import yaml

import logging

""" Some constansts, mainly mappings that would be used to format data
"""

MAPPING_GIT = {
    "mappings": {
        "item": {
            "properties": {
                "date": {
                    "type": "date",
                    "format" : "E MMM d HH:mm:ss yyyy Z",
                    "locale" : "US"
                },
                "project": {"type": "keyword"},
                "commit": {"type": "keyword"},
                "author": {"type": "keyword"},
                "domain": {"type": "keyword"},
                "file": {"type": "keyword"},
                "added": {"type": "integer"},
                "removed": {"type": "integer"},
                "repository": {"type": "keyword"}
            }
        }
    }
}

MAPPING_JIRA = {
    "mappings": {
        "item": {
            "properties": {
                "date": {
                    "type": "date",
                    "format": "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                },
                "contributor_name": {"type": "keyword"},
                "title": {"type": "string"},
                "state": {"type": "keyword"},
                "id": {"type": "keyword"},
                "closed_at": {
                    "type": "date",
                    "format": "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                },
                "time_to_solve": {"type": "integer"},
                "assignee_name": {"type": "keyword"}
            }
        }
    }
}

def read_config_file(filename):
    """ Function to read yaml file with settings information
    """

    with open(filename) as data_file:
    	config_data = yaml.load(data_file)

    logging.info(filename + ' settings file readed and parsed')
    return config_data

def establish_connection(es_host):
    """ Function to estabilish connection with a running Elasticsearch

        Given an elasticsearch host, it returns an Elasticsearch client
    """

    es = Elasticsearch([es_host])

    if not es.ping():
        raise ValueError('Connection refused')
    else:
        logging.info('Connection established with ' + es_host)
        return es

def create_ES_index(es, index_name, index_mapping):
    """ Function to create an Elasticsearch index.
        Given an ES client (es), an index name (index_name) and its mapping
        (index_mapping), it removes any index with same name and create a
        new one.
    """
    es.indices.delete(index_name, ignore=[400, 404])
    es.indices.create(index_name, body=index_mapping)
    logging.info(index_name + ' index created')

def create_KIB_index_pattern(es, index_name, time_field_name):
    """ Function to create a Kibana index pattern to visualize data
        Given an ES client (es), an index (index_name) and its time field name
        (time_field_name), it creates a Kibana index pattern to visualize
        the data.
    """
    es.index(index='.kibana', doc_type='index-pattern', id=index_name, body={'title': index_name, 'timeFieldName': time_field_name})
    logging.info(index_name + ' Kibana index pattern created')
