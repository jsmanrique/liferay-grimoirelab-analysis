# Simple Liferay Community analysis with GrimoireLab

This repo contains some scripts and utilities for analyzing current
[Liferay Community projects](https://community.liferay.com/projects/) using
some parts of [GrimoireLab](http://grimoirelab.github.io) free, open source technology.

The output in a [Kibana]() dashboard might be something similar to this:

![Basic Liferay dashboard](liferay-basic-numbers.jpg)

## What do I need to run it?

Main dependecies are:

* [GrimoireLab/Perceval](https://github.com/grimoirelab/perceval) to retrieve the data
* [Jupyter notebooks](http://jupyter.org/)
* [Elasticsearch](https://www.elastic.co/products/elasticsearch) and [Kibana](https://www.elastic.co/products/kibana). I've tested it with 5.4.x releases.
* [Elasticsearch-py](https://elasticsearch-py.readthedocs.io/) and [Elasticsearch DSL-py](https://elasticsearch-dsl.readthedocs.io/)

The projects data sources are *described* in the [liferay-community-repositories.yml](liferay-community-repositories.yml) file. If you need to update new repositories, just edit it and make the changes needed.

This file also describes where Elasticsearch is running. You can change it for your own specific parameters.

## How it works?

The procedure is quite simple:

1. Open [Liferay Analysis jupyter notebook](Liferay%20Analysis.ipynb) and follow the steps *described* there.
2. Once you have all the data in Elasticsearch, open Kibana and import [liferay-dashboard.json](liferay-dashboard.json)
3. Go to `Dashboards` and open the `Liferay` dashboard to get it

You'll be able to build and store your own visualizations. Feel free to improve it!

## Comments, issues, etc.

Feel free to [open issues](https://gitlab.com/jsmanrique/liferay-grimoirelab-analysis/issues/new) for comments

## Next steps

* ~~Add [Liferay Jira](https://issues.liferay.com/) related information~~
* Move [utils.py](utils.py) to an external library or similar. Check [GrimoireUtils](https://gitlab.com/jsmanrique/GrimoireUtils) for on-going work

# Disclaimer

The number of repositories is high, with a lot of activity, so it'll take *some time* to download and parse everything. Specially the
public JIRA with more than 100K items stored (it took me ~4 hours).

*Incremental analysis* is not implemented in this demo, so it might take a while to run the analysis again. The *good part* is
that gits might be already mainly cloned, so it would be just an update.

This is a very simple demo of [GrimoireLab](http://grimoirelab.github.io) capabilities. If you want to know more about how the
toolkit works and the rest of the pieces you might need for a more complete analysis, check [GrimoireLab Training open book](https://www.gitbook.com/book/jgbarah/grimoirelab-training/details).
